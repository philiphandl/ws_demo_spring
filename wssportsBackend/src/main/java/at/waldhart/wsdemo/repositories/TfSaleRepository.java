package at.waldhart.wsdemo.repositories;

import at.waldhart.wsdemo.dtos.TfSaleDTO;
import at.waldhart.wsdemo.models.TfSale;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TfSaleRepository extends CrudRepository<TfSale, Long> {

    List<TfSale> findAll();

    @Query("SELECT new at.waldhart.wsdemo.dtos.TfSaleDTO(sal.salNo, sal.salType, sal.salDateFrom, sal.salDateTo, sal.salAdditionalProperties, sal.salArticle.artNo, sal.salPrice.prcPrice) from TfSale sal")
    List<TfSaleDTO> findAllTfSaleDTO();

    TfSale findBySalNo(long salNo);
}
