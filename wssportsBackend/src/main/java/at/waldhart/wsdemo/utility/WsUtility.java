package at.waldhart.wsdemo.utility;

import java.lang.reflect.Field;
import java.util.List;

public class WsUtility {

    public static <T> T mergeObjects(final T first, final T second){
        Class<?> clas = first.getClass();
        Field[] fields = clas.getDeclaredFields();
        Object result = null;
        try {
            result = clas.getDeclaredConstructor().newInstance();
            for (Field field : fields) {
                field.setAccessible(true);
                Object value1 = field.get(first);
                Object value2 = field.get(second);
                Object value = (value1 != null) ? value1 : value2;
                field.set(result, value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (T) result;
    }

    public static <T> T patchObjects(final T first, final T second) {
        return (T) mergeObjects(first, second);
    }

    public static <T> T patchObjects(final T first, final T second, final List<String> deletedFields){
        Class<?> clas = first.getClass();
        Field[] fields = clas.getDeclaredFields();
        Object result = null;
        try {
            result = clas.getDeclaredConstructor().newInstance();
            for (Field field : fields) {
                field.setAccessible(true);
                Object value1 = field.get(first);
                Object value2 = field.get(second);
                Object value = null;
                if(!deletedFields.contains(field.getName())){
                    value = (value1 != null) ? value1 : value2;
                }
                field.set(result, value);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return (T) result;
    }
}
