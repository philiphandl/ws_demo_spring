package at.waldhart.wsdemo.dtos.mappers;

import at.waldhart.wsdemo.dtos.TdPriceDTO;
import at.waldhart.wsdemo.models.TdPrice;
import org.springframework.stereotype.Component;

@Component
public class TdPriceDtoMapper {
    public TdPriceDTO toDto(final TdPrice originalModel) {
        TdPriceDTO newDto = new TdPriceDTO();

        newDto.setPrcNo(originalModel.getPrcNo());
        newDto.setPrcPercentage(originalModel.getPrcPercentage());
        newDto.setPrcDateTo(originalModel.getPrcDateTo());
        newDto.setPrcDateFrom(originalModel.getPrcDateFrom());
        newDto.setPrcPrice(originalModel.getPrcPrice());
        newDto.setPrcArticle(originalModel.getPrcArticle().getArtNo());

        return newDto;
    }

}
