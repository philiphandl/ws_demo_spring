package at.waldhart.wsdemo.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class TfEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long evnNo;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="evn_art_no")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private TdArticle evnArticle;

    @NotNull
    @Type(type = "org.hibernate.type.DateType")
    private Date evnDateStartday;

    /* getter and setter */
    public Long getEvnNo() {
        return evnNo;
    }

    public void setEvnNo(final Long evnNo) {
        this.evnNo = evnNo;
    }

    public Date getEvnDateStartday() {
        return evnDateStartday;
    }

    public void setEvnDateStartday(final Date evnDateStartday) {
        this.evnDateStartday = evnDateStartday;
    }

    public TdArticle getEvnArticle() {
        return evnArticle;
    }

    public void setEvnArticle(final TdArticle evnArticle) {
        this.evnArticle = evnArticle;
    }

    // TODO: adapt with new fields
    @Override
    public String toString() {
        return "[Event#"+this.evnNo+"]: {event="+this.evnArticle.getArtName()+"(#"+
                this.evnArticle.getArtNo()+") , startdate="+this.evnDateStartday+
                "}";
    }

}
