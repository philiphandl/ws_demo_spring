package at.waldhart.wsdemo.controllers;

import at.waldhart.wsdemo.dtos.TdPriceDTO;
import at.waldhart.wsdemo.dtos.TfEventDTO;
import at.waldhart.wsdemo.dtos.mappers.TdPriceDtoMapper;
import at.waldhart.wsdemo.dtos.mappers.TfEventDtoMapper;
import at.waldhart.wsdemo.errors.exceptions.CannotUpdateNonExistingEntity;
import at.waldhart.wsdemo.models.TdArticle;
import at.waldhart.wsdemo.models.TdPrice;
import at.waldhart.wsdemo.models.TfEvent;
import at.waldhart.wsdemo.services.TdArticleService;
import at.waldhart.wsdemo.services.TdPriceService;
import at.waldhart.wsdemo.services.TfEventService;
import at.waldhart.wsdemo.utility.WsUtility;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.json.JsonObject;
import javax.json.JsonPatch;
import javax.json.JsonStructure;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/articles")
public class TdArticleController {

    @Autowired
    TdArticleService articleService;

    @Autowired
    TdPriceService TdPriceService;

    @Autowired
    TfEventService TfEventService;

    @Autowired
    TfEventDtoMapper eventDtoMapper;

    @Autowired
    TdPriceDtoMapper priceDtoMapper;

    @RequestMapping(value="/", method= RequestMethod.GET)
    @ResponseBody
    public List<TdArticle> getAllTdArticles(){
        long start = System.currentTimeMillis();
        List<TdArticle> returnPayload = this.articleService.getArticles();
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TdArticleController::getAllTdArticles]: "+(finish-start)+"ms");

        return returnPayload;
    }

    @RequestMapping(value="/{artNo}", method= RequestMethod.GET)
    @ResponseBody
    public TdArticle getTdArticle(@PathVariable final Long artNo){
        long start = System.currentTimeMillis();
        TdArticle rtn = this.articleService.getArticle(artNo);
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TdArticleController::getAllTdArticles]: "+(finish-start)+"ms");

        return rtn;
    }


    @RequestMapping(value="/{artNo}/prices", method= RequestMethod.GET)
    @ResponseBody
    public List<TdPriceDTO> getTdPricesForArticle(@PathVariable final Long artNo){
        long start = System.currentTimeMillis();
        List<TdPriceDTO> rtn = this.TdPriceService.getTdPricesForTdArticle(artNo).stream().map(priceDtoMapper::toDto).collect(toList());
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TdArticleController::getTdPricesForArticle]: "+(finish-start)+"ms");

        return rtn;
    }

    @RequestMapping(value="/{artNo}/events", method= RequestMethod.GET)
    @ResponseBody
    public List<TfEventDTO> getTfEventsForArticle(@PathVariable final Long artNo){
        long start = System.currentTimeMillis();
        List<TfEventDTO> rtn = this.TfEventService.getTfEventForTdArticle(artNo).stream().map(eventDtoMapper::toDto).collect(toList());
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TdArticleController::getTfEventsForArticle]: "+(finish-start)+"ms");

        return rtn;
    }


    @RequestMapping(value="/", method= RequestMethod.POST)
    @ResponseBody
    public TdArticle createTdArticle(@RequestBody final TdArticle newEntity){
        long start = System.currentTimeMillis();
        TdArticle rtn = this.articleService.createArticle(newEntity);
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TdArticleController::createTdArticle]: "+(finish-start)+"ms");

        return rtn;
    }

    @RequestMapping(value="/{artNo}", method= RequestMethod.PUT)
    @ResponseBody
    public TdArticle updateTdArticle(@RequestBody final TdArticle updateEntity, @PathVariable final Long artNo){
        long start = System.currentTimeMillis();
        TdArticle rtn = this.articleService.updateArticle(artNo, updateEntity);
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TdArticleController::updateTdArticle]: "+(finish-start)+"ms");

        return rtn;
    }

    @RequestMapping(value="/{artNo}", method= RequestMethod.PATCH)
    @ResponseBody
    public TdArticle patchTdArticle(@RequestBody final String patch, @PathVariable final Long artNo){
        long start = System.currentTimeMillis();
        TdArticle rtn = this.articleService.patchArticle(artNo, patch);
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TdArticleController::patchTdArticle]: "+(finish-start)+"ms");

        return rtn;
    }

    @RequestMapping(value="/{id}", method= RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Object> deleteTdArticle(@PathVariable final Long id){
        long start = System.currentTimeMillis();
        this.articleService.deleteTfArticle(id);
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TfEvent::deleteTdArticle]: "+(finish-start)+"ms");

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
