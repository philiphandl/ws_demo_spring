package at.waldhart.wsdemo.errors.exceptions;

public class CannotUpdateNonExistingEntity extends RuntimeException {
    private static final String defaultMessage = "Cannot update this entity because it does not exist yet. You can create it by making a POST request.";



    public CannotUpdateNonExistingEntity() {
        super(CannotUpdateNonExistingEntity.defaultMessage);
    }

    public CannotUpdateNonExistingEntity(final Long requestedId) {
        super(CannotUpdateNonExistingEntity.defaultMessage+" (ID="+requestedId+")");
    }

    public CannotUpdateNonExistingEntity(final Long requestedId, final Class entityClass) {
        super("The requested "+entityClass.getSimpleName()+" (Package="+entityClass.getPackageName()+") with ID="+requestedId+" cannot be updated because it does not exist. You can create it by making a POST request.");
    }
}
