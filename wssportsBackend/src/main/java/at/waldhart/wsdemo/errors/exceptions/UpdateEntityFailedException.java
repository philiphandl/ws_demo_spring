package at.waldhart.wsdemo.errors.exceptions;

public class UpdateEntityFailedException extends RuntimeException {
    private static final String defaultMessage = "An exception occured while trying to update entity...";

    private Object errornousData;

    public UpdateEntityFailedException() {
        super(UpdateEntityFailedException.defaultMessage);
    }

    public UpdateEntityFailedException(Long requestedId) {
        super(UpdateEntityFailedException.defaultMessage+" (ID="+requestedId+")");
    }

    public UpdateEntityFailedException(final Long requestedId, final Object errornousData) {
        super("The requested "+errornousData.getClass().getSimpleName()+
                " (Package="+errornousData.getClass().getPackageName()+
                ") with ID="+requestedId+
                " cannot be updated. Make sure you pass a valid data.");
        this.errornousData = errornousData;
    }

    public Object getErrornousData() {
        return this.errornousData;
    }
}
