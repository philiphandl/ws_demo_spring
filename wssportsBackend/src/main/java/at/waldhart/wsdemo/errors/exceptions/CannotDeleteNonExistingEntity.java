package at.waldhart.wsdemo.errors.exceptions;

public class CannotDeleteNonExistingEntity extends RuntimeException {
    private static final String defaultMessage = "Cannot delete this entity because it does not exist in the associated database. Have you already deleted this entity?";



    public CannotDeleteNonExistingEntity() {
        super(CannotDeleteNonExistingEntity.defaultMessage);
    }

    public CannotDeleteNonExistingEntity(final Long requestedId) {
        super(CannotDeleteNonExistingEntity.defaultMessage+" (ID="+requestedId+")");
    }

    public CannotDeleteNonExistingEntity(final Long requestedId, final Class entityClass) {
        super("The requested "+entityClass.getSimpleName()+" (Package="+entityClass.getPackageName()+") with ID="+requestedId+" cannot be deleted because it was not found in the database. Have you already deleted this one?");
    }
}
