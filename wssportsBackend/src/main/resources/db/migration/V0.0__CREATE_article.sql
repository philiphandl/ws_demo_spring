drop table if exists tf_article;

CREATE TABLE td_article (
                            ART_NO serial not null,
                            ART_NAME VARCHAR(100) NOT NULL,
                            ART_TYPE SMALLINT NOT NULL, -- 1 Gruppenkurs, ...
                            ART_INFORMATION JSONB,
                            ART_ACTIVE SMALLINT DEFAULT 0 NOT NULL
);

ALTER TABLE "public".td_article ADD CONSTRAINT pky_td_article_00 PRIMARY KEY (art_no);
