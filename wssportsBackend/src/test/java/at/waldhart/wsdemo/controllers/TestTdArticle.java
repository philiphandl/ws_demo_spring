package at.waldhart.wsdemo.controllers;

import at.waldhart.wsdemo.models.TdArticle;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class TestTdArticle {

    @Autowired
    TdArticleController articleController;

    @Test
    void testSetup() {
        assertNotNull(articleController);
    }

    @Test
    void testGetAllTdArticles(){
        List<TdArticle> testList = this.articleController.getAllTdArticles();

        System.out.println(testList);
    }
}
