import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'wssportsFrontend';

  public onRouterOutletActivate(ev: any): void {
    if(ev.title){
      this.title = ev.title;
    }
  }
}
