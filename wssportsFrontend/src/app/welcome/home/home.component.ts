import { Component, OnInit } from '@angular/core';
import {PageComponent} from "../../wssports/structure/page.component";
import {ActivatedRoute, Router} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends PageComponent implements OnInit {

  public title: string = 'Home';
  public currentEditingEvent: any = undefined;

  eventEditingForm = new FormGroup({
    eventLabel: new FormControl(''),
    startTime: new FormControl(''),
    endTime: new FormControl('')
  });

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title
  ) {
    super(router, route, titleService);
  }

  ngOnInit(): void {
  }

  showTasks(): void {
    console.log(this.instructors);
  }

  updateEventEntry(ev: any): void {
    console.log('DEBUGGGGGG----updateEventEntry', ev);
  }

  triggerEventEdit(ev: any): void {
    console.log('DEBUGGGGGGGGG----triggerEventEdit', ev);
    this.currentEditingEvent = ev;
    this.eventEditingForm.get('eventLabel')?.setValue(this.currentEditingEvent.label);
    this.eventEditingForm.get('startTime')?.setValue(this.currentEditingEvent.start);
    this.eventEditingForm.get('endTime')?.setValue(this.currentEditingEvent.end);
  }

  onEventEditingSubmit(): void {
    console.log('DEBUGGGGGG----onEventEditingSubmit', this.eventEditingForm.value);
  }

  instructors = [
    {
      id: 1,
      label: 'Reagge Gandalf',
      description: 'description for task 1',
      start: '09:00',
      end: '14:30',
      tooltip: 'Task1',
      resourceId: 10,
      eventsList: [
        {
          date: new Date('2022-04-05'),
          start: '09:30',
          end: '12:00',
          label: 'Event #1.1'
        },
        {
          date: new Date('2022-04-06'),
          start: '12:30',
          end: '14:00',
          label: 'Event #1.2',
          color: '#c7781e'
        }
      ]
    },
    {
      id: 2,
      label: 'Hiphop Gandalf',
      description: 'description for task 2',
      start: '10:00',
      end: '11:00',
      tooltip: 'Task2',
      resourceId: 12,
      eventsList: [
        {
          date: new Date('2022-04-05'),
          start: '09:30',
          end: '11:00',
          label: 'Event #2.1'
        },
        {
          date: new Date('2022-04-05'),
          start: '12:30',
          end: '14:00',
          label: 'Event #2.2'
        },
        {
          date: new Date('2022-04-05'),
          start: '15:30',
          end: '16:00',
          label: 'Event #2.3'
        }
      ]
    },
    {
      id: 3, // Unique ID
      label: 'Metal Gandalf', // is shown inside the bars on timeline
      description: 'description for task 2a',
      tooltip: 'tooltip for task', // is shown when task is hovered
      start: '10:00', // start time of the task
      end: '14:25', // end time of the task
      resourceId: 4,
      eventsList: [
        {
          date: new Date('2022-04-05'),
          start: '11:30',
          end: '12:45',
          label: 'Event #3.1',
          color: '#18BFED'
        },
        {
          date: new Date('2022-04-05'),
          start: '13:30',
          end: '14:00',
          label: 'Event #3.2'
        }
      ]
    }
  ];

  unassignedEvents = [
    {
      date: '2022-04-06',
      start: '09:30',
      end: '12:00',
      label: 'Event #1.1'
    },
    {
      date: '2022-04-05',
      start: '12:30',
      end: '14:00',
      label: 'Event #1.2'
    },
    {
      date: '2022-04-05',
      start: '09:30',
      end: '11:00',
      label: 'Event #2.1'
    },
    {
      date: '2022-04-06',
      start: '12:30',
      end: '14:00',
      label: 'Event #2.2'
    },
    {
      date: '2022-04-05',
      start: '15:30',
      end: '16:00',
      label: 'Event #2.3'
    },
    {
      date: '2022-04-05',
      start: '11:30',
      end: '12:45',
      label: 'Event #3.1'
    },
    {
      date: '2022-04-05',
      start: '13:30',
      end: '14:00',
      label: 'Event #3.2'
    },
    {
      date: '2022-04-06',
      start: '09:30',
      end: '12:00',
      label: 'Event #1.1'
    },
    {
      date: '2022-04-05',
      start: '12:30',
      end: '14:00',
      label: 'Event #1.2',
      color: '#c7781e'
    },
    {
      date: '2022-04-06',
      start: '09:30',
      end: '11:00',
      label: 'Event #2.1'
    },
    {
      date: '2022-04-05',
      start: '12:30',
      end: '14:00',
      label: 'Event #2.2'
    },
    {
      date: '2022-04-07',
      start: '15:30',
      end: '16:00',
      label: 'Event #2.3',
      color: '#d818ed'
    },
    {
      date: '2022-04-05',
      start: '11:30',
      end: '12:45',
      label: 'Event #3.1'
    },
    {
      date: '2022-04-06',
      start: '13:30',
      end: '14:00',
      label: 'Event #3.2'
    },
    {
      date: '2022-04-05',
      start: '09:30',
      end: '12:00',
      label: 'Event #1.1'
    },
    {
      date: '2022-04-06',
      start: '12:30',
      end: '14:00',
      label: 'Event #1.2'
    },
    {
      date: '2022-04-05',
      start: '09:30',
      end: '11:00',
      label: 'Event #2.1',
      color: '#18BFED'
    },
    {
      date: '2022-04-05',
      start: '12:30',
      end: '14:00',
      label: 'Event #2.2',
      color: '#b3c71e'
    },
    {
      date: '2022-04-07',
      start: '15:30',
      end: '16:00',
      label: 'Event #2.3'
    },
    {
      date: '2022-04-06',
      start: '11:30',
      end: '12:45',
      label: 'No color',

    },
    {
      date: '2022-04-06',
      start: '13:30',
      end: '14:00',
      label: 'Event #3.2'
    }
  ];
}
