import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import {WssportsModule} from "../wssports/wssports.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {InputTextModule} from "primeng/inputtext";
import {PrimeDesignModule} from "../wssports/modules/primeng-design.module";
import {CalendarModule} from "primeng/calendar";



@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    WssportsModule,
    FormsModule,
    PrimeDesignModule,
    ReactiveFormsModule,
    CalendarModule
  ]
})
export class WelcomeModule { }
