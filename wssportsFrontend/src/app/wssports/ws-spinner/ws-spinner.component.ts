import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-ws-spinner',
  templateUrl: './ws-spinner.component.html',
  styleUrls: ['./ws-spinner.component.scss']
})
export class WsSpinnerComponent implements OnInit {

  @Input() loading: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
