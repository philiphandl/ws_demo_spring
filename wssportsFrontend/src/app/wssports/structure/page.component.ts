import { Title } from '@angular/platform-browser';
import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter, map, mergeMap } from 'rxjs/operators';

@Component({
  template: ''
})
export abstract class PageComponent implements OnDestroy {

  /** The subscription for listening to navigation events */
  private routerSubscription: Subscription | undefined;

  /** The title being shown in the browser tab */
  title: string = '';

  /** Constructor */
  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _titleService: Title
  ) {
    this.setTitleOnNavigation();
  }

  /**
   * Listens to navigation events and when the page has been opened, i.e. navigated to, the title in the browser tab is updated with the title contained in the data property of the navigation event. This title is defined in the `app-routing.module.ts` in the main directory of the app.
   */
  private setTitleOnNavigation(): void {
    this.routerSubscription = this._router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => this._activatedRoute),
        map(route => {
          while (route.firstChild) route = route.firstChild;
          return route;
        }),
        filter(route => route.outlet === 'primary'),
        mergeMap(route => route.data)
      )
      .subscribe((event) => this.updateTitle(event['title']));
  }

  /**
   * Sets the title in the browser tab to the title provided as a parameter and sets `this.title` to the same value.
   * @param title - a string title
   */
  public updateTitle(title: string): void {
    this._titleService.setTitle(title);
    this.title = title;
  }

  ngOnDestroy() {
    if(this.routerSubscription !== undefined){
      this.routerSubscription.unsubscribe();
    }
  }
}
