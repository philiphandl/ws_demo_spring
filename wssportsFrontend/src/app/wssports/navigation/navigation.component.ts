import { Component, OnInit } from '@angular/core';

class MenuItem{
  path: string = '';
  title: string = '';

  constructor(path: string = '', title: string = ''){
    this.path = path;
    this.title = title;
  }
}

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  public menuItems: MenuItem[] = [
    new MenuItem('/home', 'Home'),
    new MenuItem('/articles', 'Artikel'),
    new MenuItem('/events', 'Events'),
    new MenuItem('/prices', 'Preise'),
    new MenuItem('/sales', 'Verkäufe')

  ];
  constructor() { }

  ngOnInit(): void {
  }

}
