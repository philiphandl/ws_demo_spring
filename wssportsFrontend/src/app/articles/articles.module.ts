import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleComponent } from './article/article.component';
import {ButtonModule} from "primeng/button";
import {PrimeDesignModule} from "../wssports/modules/primeng-design.module";
import { EventComponent } from './event/event.component';
import { PriceComponent } from './price/price.component';
import {WssportsModule} from "../wssports/wssports.module";

const articles_prefix = 'articles/';
const events_prefix = 'events/';
const prices_prefix = 'prices/';

@NgModule({
  declarations: [
    ArticleComponent,
    EventComponent,
    PriceComponent
  ],
  exports: [
    ArticleComponent
  ],
    imports: [
        CommonModule,
        PrimeDesignModule,
        WssportsModule
    ],
  providers: [
    {
      provide: 'articles_prefix',
      useValue: articles_prefix
    },
    {
      provide: 'events_prefix',
      useValue: events_prefix
    },
    {
      provide: 'prices_prefix',
      useValue: prices_prefix
    },
  ]
})
export class ArticlesModule { }
