import { Component, OnInit } from '@angular/core';
import {PageComponent} from "../../wssports/structure/page.component";
import {ActivatedRoute, Router} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {ArticlesService} from "../articles.service";
import {first} from "rxjs/operators";
import {Article, WsEvent} from "../models";

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent extends PageComponent implements OnInit {

  public title: string = 'Events';
  public eventsLoading: boolean = false;
  public allEvents: WsEvent[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title,
    private articlesService: ArticlesService
  ) {
    super(router, route, titleService);
  }

  ngOnInit(): void {
    this.eventsLoading = true;
    this.articlesService.getEvents().pipe(first()).subscribe(
      resp => {
        console.log('DEBUGGGGGGG----getEvents', resp);
        if(resp && resp.length > 0){
          this.allEvents = resp;
        }
        else{
          this.allEvents = [];
        }

      },
      err => {
        console.log('DEBUGGGGGGG-----getArticles---ERROR!', err);
      }
    ).add(() => {this.eventsLoading = false;});
  }

}
