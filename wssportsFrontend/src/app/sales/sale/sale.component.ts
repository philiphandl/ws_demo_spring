import { Component, OnInit } from '@angular/core';
import {PageComponent} from "../../wssports/structure/page.component";
import {Article} from "../../articles/models";
import {ActivatedRoute, Router} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {ArticlesService} from "../../articles/articles.service";
import {first} from "rxjs/operators";
import {WsSale} from "../models";
import {SalesService} from "../sales.service";

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.scss']
})
export class SaleComponent extends PageComponent implements OnInit {

  public title: string = 'Verkäufe';
  public salesLoading: boolean = false;
  public allSales: WsSale[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title,
    private salesService: SalesService
  ) {
    super(router, route, titleService);
  }

  ngOnInit(): void {
    this.salesLoading = true;
    this.salesService.getSales().pipe(first()).subscribe(
      resp => {
        console.log('DEBUGGGGGGG----getArticles', resp);
        if(resp && resp.length > 0){
          this.allSales = resp;
        }
        else{
          this.allSales = [];
        }

      },
      err => {
        console.log('DEBUGGGGGGG-----getArticles---ERROR!', err);
      }
    ).add(() => {this.salesLoading = false;});
  }

}
