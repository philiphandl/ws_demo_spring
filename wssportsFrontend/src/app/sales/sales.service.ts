import {Injectable, Injector} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Article} from "../articles/models";
import {environment} from "../../environments/environment";
import {WsSale} from "./models";

@Injectable({
  providedIn: 'root'
})
export class SalesService {

  constructor(
    private injector: Injector,
    private http: HttpClient) { }

  public getSales(): Observable<WsSale[]> {
    return this.http.get<WsSale[]>(environment.apiBaseUrl + this.injector.get('sales_prefix'));
  }
}
