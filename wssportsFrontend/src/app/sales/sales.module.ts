import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaleComponent } from './sale/sale.component';
import {PrimeDesignModule} from "../wssports/modules/primeng-design.module";
import {WssportsModule} from "../wssports/wssports.module";

const sales_prefix = 'sales/';

@NgModule({
  declarations: [
    SaleComponent
  ],
    imports: [
        CommonModule,
        PrimeDesignModule,
        WssportsModule
    ],
  providers: [
    {
      provide: 'sales_prefix',
      useValue: sales_prefix
    }
  ]
})
export class SalesModule { }
