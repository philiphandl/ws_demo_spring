import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ArticlesModule} from "./articles/articles.module";
import {WelcomeModule} from "./welcome/welcome.module";
import {WssportsModule} from "./wssports/wssports.module";
import {HttpClientModule} from "@angular/common/http";
import {ButtonModule} from "primeng/button";
import {PrimeDesignModule} from "./wssports/modules/primeng-design.module";
import {SalesModule} from "./sales/sales.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ArticlesModule,
    SalesModule,
    WelcomeModule,
    WssportsModule,
    PrimeDesignModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
