#!/bin/bash
echo "###############################"
echo "### START WS DEMO BENCHMARK ###"
echo "###############################"

BASEURL=$1

bash benchmark_post_articles.sh $BASEURL 1000
bash benchmark_post_events.sh $BASEURL
bash benchmark_post_prices.sh $BASEURL
bash benchmark_post_sales.sh $BASEURL
bash benchmark_get.sh $BASEURL
bash benchmark_get_joins.sh $BASEURL