#!/bin/bash
echo "####################################"
echo "### BENCHMARK GET REQUESTS (ALL) ###"
echo "####################################"

make_request(){
	if [ -z "$4" ]
	then
		start=`date +%s.%N`
		curl --location --request $2 $3 -s -o benchmark_output.txt
		end=`date +%s.%N`
	else
		start=`date +%s.%N`
		curl --location --request $2 $3 -s -o benchmark_output.txt --header 'Content-Type: application/json' --data-raw "$4"
		end=`date +%s.%N`
	fi

	

	runtime=$( echo "$end - $start" | bc -l )
	echo $3 ';' $2 ';' $runtime >> $1
}
export -f make_request

# reset output file
RESULTFILE=results/results_benchmark_get.csv
rm $RESULTFILE
touch $RESULTFILE

# benchmark GET requests
echo 'route;method;runtime' >> $RESULTFILE


routestotest=($1'/api/articles/' $1'/api/prices/' $1'/api/sales/' $1'/api/events/')

start_all=`date +%s.%N`
for r in ${routestotest[@]}
do
	for y in {1..10}
	do
		make_request $RESULTFILE 'GET' $r
	done
done
end_all=`date +%s.%N`
runtime=$( echo "$end_all - $start_all" | bc -l )

echo 'ALL;GET;' $runtime >> $RESULTFILE


# benchmark parallel calls
start_parallel_all=`date +%s.%N`
for r in ${routestotest[@]}
do
	start_parallel_loop=`date +%s.%N`
	seq 1 100 | xargs -n1 -P8 bash -c 'make_request "$@"' _ $RESULTFILE "GET" $r
	end_parallel_loop=`date +%s.%N`
	runtime=$( echo "$end_parallel_loop - $start_parallel_loop" | bc -l )
	echo $r ';GET;' $runtime >> results_benchmark_get.csv
done
end_parallel_all=`date +%s.%N`
runtime=$( echo "$end_parallel_all - $start_parallel_all" | bc -l )
echo 'PARALLEL;GET;' $runtime >> $RESULTFILE
