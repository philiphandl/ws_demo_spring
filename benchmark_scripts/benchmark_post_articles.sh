#!/bin/bash
echo "##########################################"
echo "### BENCHMARK POST REQUESTS (ARTICLES) ###"
echo "##########################################"

make_request(){
	if [ -z "$4" ]
	then
		start=`date +%s.%N`
		curl --location --request $2 $3 -s -o benchmark_output.txt
		end=`date +%s.%N`
	else
		start=`date +%s.%N`
		curl --location --request $2 $3 -s -o benchmark_output.txt --header 'Content-Type: application/json' --data-raw "$4"
		end=`date +%s.%N`
	fi	

	runtime=$( echo "$end - $start" | bc -l )
	echo $3 ';' $2 ';' $runtime >> $1
}
export -f make_request

# reset output file
RESULTFILE=results/results_benchmark_post_articles.csv
rm $RESULTFILE
touch $RESULTFILE

# benchmark GET requests
echo 'route;method;runtime' >> $RESULTFILE

start_all=`date +%s.%N`
for i in $(seq "$2")
do
	artType=$(($i % 4))
	artActive=$(($i % 2))
	make_request $RESULTFILE POST $1'/api/articles/' '{"artName": "BENCHMARK_TES '$i'",
			"artType": '$artType',
			"artActive": '$artActive',
			"artInformation": [
				{
					"text": {
						"cs": "<p>CS Leistung '$i'</p>",
						"de": "<ul><li><p>Leistung '$i'</p></li><li><p>Leistung 2</p></li></ul>"
					},
					"headline": {
						"cs": "Leistungen CS",
						"da": "Leistungen DA",
						"de": "Leistungen"
					}
				},
				{
					"text": {
						"de": "<p>Nullam cursus lacinia erat. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Vestibulum fringilla pede sit amet augue.</p>"
					},
					"headline": {
						"de": "Leistungen '$i'"
					}
				}
			]
		}'
done

end_all=`date +%s.%N`
runtime=$( echo "$end_all - $start_all" | bc -l )

echo 'ALL;GET;' $runtime >> $RESULTFILE
echo 'ALL;GET;' $runtime


start_all=`date +%s.%N`
seq 1 800 | xargs --replace={} -n1 -P8 bash -c 'make_request "$@"' _ $RESULTFILE POST $1'/api/articles/' '{"artName": "BENCHMARK_TES PARALLEL '{}'",
			"artType": '{}',
			"artActive": 1,
			"artInformation": [
				{
					"text": {
						"cs": "<p>CS Leistung PARALLEL '{}'</p>",
						"de": "<ul><li><p>Leistung PARALLEL '{}'</p></li><li><p>Leistung 2</p></li></ul>"
					},
					"headline": {
						"cs": "Leistungen CS",
						"da": "Leistungen DA",
						"de": "Leistungen"
					}
				},
				{
					"text": {
						"de": "<p>Nullam cursus lacinia erat. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Vestibulum fringilla pede sit amet augue.</p>"
					},
					"headline": {
						"de": "Leistungen PARALLEL '{}'"
					}
				}
			]
		}'

end_all=`date +%s.%N`
runtime=$( echo "$end_all - $start_all" | bc -l )
echo 'PARALLEL;POST;' $runtime >> $RESULTFILE
echo 'PARALLEL;POST;' $runtime
