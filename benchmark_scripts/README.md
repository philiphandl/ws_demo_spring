## How to run the benchmark

Just start your server that is accessible under <SERVER-BASE-URL>, e.g. `http://localhost:8080`

Then you can start either individual benchmarks or run all by:

```
bash benchmark_all.sh <SERVER-BASE-URL>

bash benchmark_all.sh http://localhost:8080
```

Afterwards all results are stored in the results directory
