#!/bin/bash
echo "#########################################"
echo "### BENCHMARK POST REQUESTS (EVENTS)  ###"
echo "#########################################"

make_request(){
	if [ -z "$4" ]
	then
		start=`date +%s.%N`
		curl --location --request $2 $3 -s -o benchmark_output.txt
		end=`date +%s.%N`
	else
		start=`date +%s.%N`
		curl --location --request $2 $3 -s -o benchmark_output.txt --header 'Content-Type: application/json' --data-raw "$4"
		end=`date +%s.%N`
	fi	

	runtime=$( echo "$end - $start" | bc -l )
	echo $3 ';' $2 ';' $runtime >> $1
}
export -f make_request

# reset output file
RESULTFILE=results/results_benchmark_post_events.csv
rm $RESULTFILE
touch $RESULTFILE

# benchmark GET requests
echo 'route;method;runtime' >> $RESULTFILE

art=$(seq 1 3)
years=$(seq 2021 2022)
months=("01" "02" "03" "04" "05" "06" "07" "08" "09" "10" "11" "12")
thirtylist=("01" "02" "03" "04" "05" "06" "07" "08" "09" "10" "11" "12" "13" "14" "15" "16" "17" "18" "19" "20" "21" "22" "23" "24" "25" "26" "27" "28" "29" "30")
thirtyonelist=("01" "02" "03" "04" "05" "06" "07" "08" "09" "10" "11" "12" "13" "14" "15" "16" "17" "18" "19" "20" "21" "22" "23" "24" "25" "26" "27" "28" "29" "30" "31")
twentyeightlist=("01" "02" "03" "04" "05" "06" "07" "08" "09" "10" "11" "12" "13" "14" "15" "16" "17" "18" "19" "20" "21" "22" "23" "24" "25" "26" "27" "28")
declare -A days
days["01"]=31
days["02"]=28
days["03"]=31
days["04"]=30
days["05"]=31
days["06"]=30
days["07"]=31
days["08"]=31
days["09"]=30
days["10"]=31
days["11"]=30
days["12"]=31

start_all=`date +%s.%N`
for y in $years
do
	for m in ${months[@]}
	do
		dayslist=${thirtyonelist[@]}
		if [ ${days[$m]} -eq 30 ] 
		then
			dayslist=${thirtylist[@]}
		fi
		
		if [ ${days[$m]} -eq 28 ] 
		then
			dayslist=${twentyeightlist[@]}
		fi
		
		for d in ${dayslist[@]}
		do		
			make_request $RESULTFILE POST $1'/api/events/' '{"evnDateStartday": "'$y'-'$m'-'$d'", "evnArticle": {"artNo": 1}}'
		done
	done	
done


end_all=`date +%s.%N`
runtime=$( echo "$end_all - $start_all" | bc -l )
echo 'ALL;POST;' $runtime >> $RESULTFILE
echo 'ALL;POST;' $runtime

start_all=`date +%s.%N`
for y in $years
do
	for m in ${months[@]}
	do
		dayslist=${thirtyonelist[@]}
		if [ ${days[$m]} -eq 30 ] 
		then
			dayslist=${thirtylist[@]}
		fi
		
		if [ ${days[$m]} -eq 28 ] 
		then
			dayslist=${twentyeightlist[@]}
		fi
		
		for d in ${dayslist[@]}
		do		
			seq 1 8 | xargs --replace={} -n1 -P8 bash -c 'make_request "$@"' _ $RESULTFILE POST $1'/api/events/' '{"evnDateStartday": "'$y'-'$m'-'$d'", "evnArticle": {"artNo": '{}'}}'
		done
	done	
done

end_all=`date +%s.%N`
runtime=$( echo "$end_all - $start_all" | bc -l )
echo 'PARALLEL;POST;' $runtime >> $RESULTFILE
echo 'PARALLEL;POST;' $runtime

