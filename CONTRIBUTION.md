# Contribution Guide

In this guide some "development-etiquettes" are defined

## GIT

Git messages should follow the following scheme: 

```
[AREA](module): <message>
```

Where:
* AREA: is the major part where the change happened: `BE`, `FE`, `DB`, `REPO`
* module: specifies in more detail which project part was affected: `authentication`, `addresses`,...
* message: describes which changes were actually made

