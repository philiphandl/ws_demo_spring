# Installation Guide:

WIth the help of this guide the setup of this project should be rather easy.
You can find a sample of the `application.properties` file in the folder `/scripts/.application.properties`


## Setting up PostgreSQL database

First of all you have to make sure postgres is installed on your machine:

```
postgres -V
```

If none is installed, change it by running the following command:

```
sudo apt install postgresql postgresql-contrib
```

Afterwards you can create a new database cluster in your project folder and start postgres:

```
pg_ctl -D pgsql/data initdb               # create cluster
pg_ctl -D pgsql/data -l db_log start      # start postgres
```

With postgres running you can create a database now:

```
createuser -d -P <user_name>;              # create role and enter passwordcreate cluster
createdb -O <user_name> <db_name>;         # create db with <user_name> as user
```

We have used so far `wsdba` for `<user_name>` and `ws_sports_db` for `<db_name>`

> **HINT:**
> In case you want to connect via command line to the database use these commands:<br>
> ```
> psql -p 5432 -h 127.0.0.1 postgres     # connect to database
> \c <db_name> <user_name>               # select you database 
> ```

Afterwards you have to add your credentials into the `wssportsBackend/src/main/resources/application.properties` file.
A sample is provided in `scripts/.application.properties`

## Start Backend Server:
Afterwards you can start the Spring-Boot server by running the following command 
in the `wssportsBackend` folder

```shell
cd wssportsBackend/
mvn spring-boot:run
```

Then you can access the backend by `http://localhost:8080/api/`